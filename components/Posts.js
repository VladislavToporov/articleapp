import React, {Component} from "react";
import {StyleSheet, Text, View, ActivityIndicator, FlatList} from "react-native";
import {HOST} from '../constants/Constant';
import {TOKEN} from '../constants/Constant'
import {RaisedTextButton} from 'react-native-material-buttons';

export default class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {isLoading: true, Post: {}}
    }

    componentDidMount() {
        console.log(HOST);
        return fetch(HOST + 'api/posts', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': TOKEN,
            },
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.filter(x => x.title),
                }, function () {
                    console.log("loading complete");
                });

            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        const {navigation} = this.props;
        if (!TOKEN) {
            this.props.navigation.navigate("Login");
        }
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        else {
            return (
                <View behavior="padding" style={styles.container}>
                    <RaisedTextButton title='New' titleColor='white'
                                      color='#3399ff'
                                      onPress={() => this.props.navigation.navigate("Create", {post: {}})}/>
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({item}) =>
                            <View style={styles.container}>
                                <View
                                    style={{
                                        borderBottomColor: 'grey',
                                        borderBottomWidth: 1,
                                        width: '100%'
                                    }}
                                />
                                <Text style={styles.headline}>{item.title}</Text>
                                <Text>{item.content}</Text>
                                <RaisedTextButton title='Details' titleColor='white'
                                                  color='#3399ff'
                                                  onPress={() => this.props.navigation.navigate("Details",
                                                      {
                                                          post: {
                                                              id: item._id,
                                                              title: item.title,
                                                              content: item.content,
                                                              image: item.image,
                                                              userId: item.userId
                                                          },
                                                          userId: item.userId
                                                      })}/>
                            </View>}
                        keyExtractor={(item, index) => index}
                    />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 20,
    },
    headline: {
        marginTop: 10,
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center'
    }
});

