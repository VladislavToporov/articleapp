import React, { Component } from "react";
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    Image,
    KeyboardAvoidingView,
} from "react-native";
import {HOST} from '../constants/Constant'

import {RaisedTextButton} from 'react-native-material-buttons';

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: "",
            Name: "",
            Password: "",
            Password_confirmation: "",
            SignUpMessage: ""
        };
    }

    setPassword(password) {
        return this.state.Password = password;
    }


    setEmail(email) {
        return this.state.Email = email;
    }
    setName(name) {
        return this.state.Name = name;
    }


    setPassword_confirmation(password_confirmation) {
        return this.state.Password_confirmation = password_confirmation;
    }

    async onRegisterPress() {
        const { Email, Password, Name, Password_confirmation } = this.state;
        console.log(Email);
        console.log(Name);
        console.log(Password);

        if (Password !== Password_confirmation) {
            console.log("passwords not equals");
            return;
        }
        console.log("password matches");
        fetch(HOST + 'api/auth/register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: Email,
                name: Name,
                password: Password
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if ((responseJson.auth) === true) {
                    this.state.SignUpMessage = "";
                    this.props.navigation.navigate("Login");
                }
                else {
                    this.state.SignUpMessage = "Пользователь уже существует";
                    console.log("sign up fail");
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={styles.keyboard}>
                    <Text>{this.state.SignUpMessage}</Text>
                    <TextInput
                        onChangeText={name =>this.setName(name)}
                        style={styles.input}
                        placeholder="Name"
                        returnKeyType="next"
                        onSubmitEditing={() => this.emailInput.focus()}
                    />
                    <TextInput
                        onChangeText={email => this.setEmail(email)}
                        style={styles.input}
                        returnKeyType="next"
                        ref={input => (this.emailInput = input)}
                        onSubmitEditing={() => this.passwordCInput.focus()}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoCorrect={false}
                        placeholder="Email"
                    />
                    <TextInput
                        onChangeText={password => this.setPassword(password)}
                        style={styles.input}
                        placeholder="Password"
                        secureTextEntry={true}
                        ref={input => (this.passwordCInput = input)}
                        onSubmitEditing={() => this.passwordInput.focus()}
                        returnKeyType="next"
                    />
                    <TextInput
                        onChangeText={password_confirmation => this.setPassword_confirmation(password_confirmation)}
                        style={styles.input}
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                        returnKeyType="go"
                        ref={input => (this.passwordInput = input)}
                    />
                </KeyboardAvoidingView>
                <RaisedTextButton style={{margin: 40}} title='Sign up ' titleColor='white' color='#3399ff'
                                  onPress={this.onRegisterPress.bind(this)}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 20,
        paddingTop: 100
    },

    input: {
        height: 40,
        // width: 300,
        marginBottom: 10,
        backgroundColor: "rgba(255,255,255,0.2)",
        // color: "#fff",
        paddingHorizontal: 10,
        borderColor: 'gray',
        borderWidth: 1
    },
    keyboard:{
        margin: 20,
        padding: 20,
        alignSelf: "stretch"
    },
});