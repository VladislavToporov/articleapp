export default class Posts extends Component {

    render() {
        const {navigation} = this.props;
        return (
            <View behavior="padding" style={styles.container}>
                <RaisedTextButton title='New' titleColor='white' color='#3399ff' onPress={() => this.props.navigation.navigate("Create", {post: {}})}/>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({item}) => <View style={styles.container}>
                            <View style={{borderBottomColor: 'grey', borderBottomWidth: 1, width: '100%'}}/>
                            <Text style={styles.headline}>{item.title}</Text>
                            <Text>{item.content}</Text>
                            <RaisedTextButton title='Details' titleColor='white' color='#3399ff' onPress={() => this.props.navigation.navigate("Details",
                                                  {post: {id: item._id, title: item.title, content: item.content, image: item.image, userId: item.userId},})}/>
                        </View>}
                    keyExtractor={(item, index) => index}
                />
            </View>
        );
    }
}