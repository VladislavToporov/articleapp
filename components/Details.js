import React, {Component} from "react";
import {StyleSheet, Text, View, ActivityIndicator, FlatList, Image} from "react-native";
import {HOST, setToken, TOKEN, USERID} from '../constants/Constant';
import {RaisedTextButton} from 'react-native-material-buttons';

export default class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {isLoading: true, Post: {}, USERNAME: ""};
    }

    setPost(post) {
        return this.state.Post = post;
    }

    setUsername(username) {
        return this.state.USERNAME = username;
    }

    componentDidMount() {
        fetch(HOST + 'api/users/' + USERID, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': TOKEN
            },
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    isLoading: false,
                    USERNAME: responseJson.name
                })
            })
            .catch((error) => {
                console.error(error);
            });
    }


    async onDeletePress() {
        console.log(this.state.Post.userId);
        console.log(USERID);
        console.log(this.state.Post);
        console.log("PostID: " + this.state.Post.id);
        if (this.state.Post.userId && this.state.Post.userId !== USERID) {
            alert('You can delete only own post ');
            return;
        }
        console.log("delete pressed");
        console.log(HOST + 'api/posts/' + this.state.Post.id);
        fetch(HOST + 'api/posts/' + this.state.Post.id, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': TOKEN,
            },
        });
        this.props.navigation.navigate("Home");
    }

    async onEditPress() {
        console.log(this.state.UserId);
        console.log(USERID);
        if (this.state.Post.userId !== USERID) {
            alert('You can edit only own post ');
            return;
        }
        console.log("edit pressed");
        this.props.navigation.navigate("Create", {post: Post});
    }

    render() {
        if (!TOKEN) {
            this.props.navigation.navigate("Login");
        }
        const {navigation} = this.props;
        this.setPost(navigation.getParam('post', {}));
        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        else {
            return (
                <View style={styles.container}>
                    <Text style={styles.headline}>{this.state.Post.title}</Text>
                    <Text style={styles.headline}>Author: {this.state.USERNAME}</Text>
                    <Image
                        defaultSource={require("../assets/images/noImage.jpg")}
                        source={{
                            uri: this.state.Post.image !== "" ? this.state.Post.image :
                                "https://vignette.wikia.nocookie.net/roblox-phantom-forces/images/7/7c/Noimage.png/revision/latest?cb=20171115203949"
                        }}
                        style={{backgroundColor: '#f1eff0', height: 150, width: 200}}/>
                    <Text style={{margin: 40}}>{this.state.Post.content}</Text>
                    <RaisedTextButton style={{marginTop: 40}} title='Delete' titleColor='white' color='#3399ff'
                                      onPress={this.onDeletePress.bind(this)}/>
                    {/*<RaisedTextButton style={{marginTop: 40}} title='Edit' titleColor='white' color='#3399ff'*/}
                    {/*onPress={this.onEditPress.bind(this)}/>*/}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 20,
    },
    logoContainer: {
        alignItems: "center",
        flexGrow: 1,
        justifyContent: "center",
    },
    headline: {
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center'
    },
    logo: {
        width: 200,
        height: 150
    },
});

