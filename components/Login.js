import React, {Component} from "react";
import {
    AppRegistry,
    KeyboardAvoidingView,
    TouchableOpacity,
    AsyncStorage,
    Image,
    TextInput,
    StyleSheet, // CSS-like styles
    Text, // Renders text
    View // Container component
} from "react-native";

import {HOST} from '../constants/Constant'
import {TOKEN} from '../constants/Constant'
import {setToken, setUsername, setUserId} from '../constants/Constant'
import {RaisedTextButton} from 'react-native-material-buttons';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: "",
            Password: "",
            SignInMesssage: ""
        };
    }

    setPassword(password) {
        return this.state.Password = password;
    }


    setEmail(email) {
        return this.state.Email = email;
    }


    async onLoginPress() {
        const {Email, Password} = this.state;
        console.log(Email);
        console.log(Password);

        fetch(HOST + 'api/auth/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: Email,
                password: Password
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson.auth);
                if (responseJson.auth === true) {
                    setToken(responseJson.token);
                    console.log(responseJson.token);
                    this.state.SignInMesssage = "";

                    fetch(HOST + 'api/auth/me', {
                        method: 'GET',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            'x-access-token': TOKEN,
                        },
                    })
                        .then((response) => response.json())
                        .then((responseJson) => {
                            console.log(responseJson);
                            console.log("user saved");
                            setUsername(responseJson.name);
                            setUserId(responseJson._id);
                            console.log(responseJson.name);
                            console.log(responseJson._id);
                        })
                        .catch((error) => {
                            console.error(error);
                        });

                    this.props.navigation.navigate("Posts");
                }
                else {
                    this.state.SignInMesssage = "Пользователь не найден";
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        const {navigation} = this.props;
        if (TOKEN) {
            this.props.navigation.navigate("Posts");
        }

        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={styles.keyboard}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logo} source={require("../assets/images/login.png")}/>
                    </View>
                    <Text>{this.state.SignInMessage}</Text>
                    <TextInput
                        placeholder="Username"
                        returnKeyType="next"
                        onSubmitEditing={() => this.contentInput.focus()}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={styles.input}
                        onChangeText={email => this.setEmail(email)}
                    />
                    <TextInput
                        placeholder="Password"
                        returnKeyType="go"
                        secureTextEntry
                        ref={input => (this.contentInput = input)}
                        style={styles.input}
                        onChangeText={password => this.setPassword(password)}
                    />

                    <RaisedTextButton style={{marginTop: 40}} title='Sign in ' titleColor='white' color='#3399ff'
                                      onPress={this.onLoginPress.bind(this)}/>
                    <RaisedTextButton style={{marginTop: 40}} title='Sign up ' titleColor='white' color='#3399ff'
                                      onPress={() => this.props.navigation.navigate("Register")}/>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 20,
    },

    logoContainer: {
        alignItems: "center",
        flexGrow: 1,
        justifyContent: "center",
    },
    logo: {
        width: 200,
        height: 200
    },
    input: {
        height: 40,
        // width: 300,
        marginBottom: 10,
        // color: "#fff",
        paddingHorizontal: 10,
        borderColor: 'gray',
        borderWidth: 1
    },
    keyboard: {
        margin: 20,
        padding: 20,
        alignSelf: "stretch"
    },
});
