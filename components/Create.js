import React, {Component} from "react";
import {
    AppRegistry,
    KeyboardAvoidingView,
    TextInput,
    StyleSheet, // CSS-like styles
    Text, // Renders text
    View // Container component
} from "react-native";

import {HOST, TOKEN, USERID} from '../constants/Constant'
import {RaisedTextButton} from 'react-native-material-buttons';

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Title: "",
            Content: "",
            Image: "",
        };
    }

    setTitle(title) {
        return this.state.Title = title;
    }

    setContent(content) {
        return this.state.Content = content;
    }

    setImage(image) {
        return this.state.Image = image;
    }

    async onSubmitPress() {
        const {Title, Content, Image} = this.state;
        console.log(Title);
        console.log(Content);
        console.log(Image);
        console.log("USERID:" + USERID);
        console.log("Token:" + TOKEN);
        fetch(HOST + 'api/posts', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': TOKEN
            },
            body: JSON.stringify({
                title: Title,
                content: Content,
                image: Image,
                userId: USERID
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
        this.props.navigation.navigate("Home");

    }

    render() {
        const {navigation} = this.props;
        if (!TOKEN) {
            this.props.navigation.navigate("Login");
        }
        // this.state.Post = navigation.getParam('post', {});
        // if (this.state.Post) {
        //     this.state.Title = this.state.post.Title;
        //     this.state.Content = this.state.post.Content;
        //     this.state.Image = this.state.post.Image;
        // }
        return (
            <View style={styles.container}>
                <KeyboardAvoidingView style={styles.keyboard}>
                    <TextInput
                        placeholder="Title"
                        // placeholderTextColor="rgba(0,0,0,1)"
                        returnKeyType="next"
                        onSubmitEditing={() => this.contentInput.focus()}
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={styles.input}
                        onChangeText={title => this.setTitle(title)}
                        editable={true}
                    />
                    <TextInput
                        placeholder="Content"
                        multiline={true}
                        // placeholderTextColor="rgba(0,0,0,1)"
                        returnKeyType="next"
                        onSubmitEditing={() => this.imageInput.focus()}
                        ref={input => (this.contentInput = input)}
                        style={styles.textarea}
                        onChangeText={content => this.setContent(content)}
                        editable={true}
                        maxLength={500}
                        numberOfLines={10}
                    />

                    <TextInput
                        placeholder="Image"
                        // placeholderTextColor="rgba(0,0,0,1)"
                        returnKeyType="go"
                        ref={input => (this.imageInput = input)}
                        style={styles.input}
                        onChangeText={image => this.setImage(image)}
                        editable={true}
                    />

                    <RaisedTextButton style={{marginTop: 40}} title='Submit ' titleColor='white' color='#3399ff'
                                      onPress={this.onSubmitPress.bind(this)}/>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 20,
    },
    input: {
        height: 40,
        // width: 300,
        marginBottom: 10,
        // color: "#fff",
        paddingHorizontal: 10,
        borderColor: 'gray',
        borderWidth: 1
    },
    textarea: {
        marginBottom: 10,
        paddingHorizontal: 10,
        borderColor: 'gray',
        borderWidth: 1
    },
    keyboard: {
        padding: 20,
        alignSelf: "stretch"
    },
});
