import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Button
} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import {StackNavigator} from 'react-navigation';

import Login from "./components/Login";
import Posts from "./components/Posts";
import Register from "./components/Register";
import Details from "./components/Details";
import Create from "./components/Create";
import {RaisedTextButton} from 'react-native-material-buttons';
import {HOST, setToken, TOKEN} from "./constants/Constant";


class Home extends Component {
    constructor(props) {
        super(props);
    }
    async onLogoutPress() {
        console.log("logout");
        setToken("");
        console.log(TOKEN);
        this.props.navigation.navigate("Login");
        fetch(HOST + 'api/auth/logout', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': TOKEN
            },
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                setToken("");
                console.log("logout success");
                this.props.navigation.navigate("Login");
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <RaisedTextButton style={{marginTop: 40}} title='Sign in' titleColor='white' color='#3399ff'
                                  onPress={() => this.props.navigation.navigate("Login")}/>
                <RaisedTextButton style={{marginTop: 40}} title='Sign up' titleColor='white' color='#3399ff'
                                  onPress={() => this.props.navigation.navigate("Register")}/>
                <RaisedTextButton style={{marginTop: 40}} title='Open posts' titleColor='white' color='#3399ff'
                                  onPress={() => this.props.navigation.navigate("Posts")}/>

                <RaisedTextButton style={{marginTop: 40}} title='Logout' titleColor='white' color='#3399ff'
                                  onPress={() => setToken("")}/>
            </View>
        );
    }
}

const RootStack = createStackNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                title: "Home"
            }
        },
        Login: {
            screen: Login,
            navigationOptions: {
                title: "Sign In"
            }
        },
        Register: {
            screen: Register,
            navigationOptions: {
                title: "Sign Up"
            }
        },
        Details: {
            screen: Details,
            navigationOptions: {
                title: "Details"
            }
        },
        Create: {
            screen: Create,
            navigationOptions: {
                title: "Create Post"
            }
        },
        // ForgetPassword: {
        //     screen: ForgetPassword,
        //     navigationOptions: {
        //         title: "Forget Password"
        //     }
        // },
        Posts: {
            screen: Posts,
            navigationOptions: {
                title: "Posts"
            }
        }
    },
    {
        initialRouteName: 'Home',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#f4511e',
                elevation: null

            },
            // headerRight: (
            //     <Button
            //         onPress={onLogoutPress().bind(this)}
            //         title="Logout"
            //         color="#dd481a"
            //     />
            // ),
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }
);

export default class App extends React.Component {
    render() {
        return <RootStack/>;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        padding: 20,
        paddingTop: 100
    }
});