export const HOST = "https://mysterious-stream-86340.herokuapp.com/";
export var TOKEN;
export var USERID;
export var USERNAME;

export function setToken(newValue) {
    console.log(newValue);
    TOKEN = newValue;
}

export function setUsername(newValue) {
    USERNAME = newValue;
}

export function setUserId(newValue) {
    USERID = newValue;
}